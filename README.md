Welcome to this tiny tutorial showing you what is a **Routing Protocol**,
how it (basically) works and also how you can configure it to route packets
from one point of the internet to another.


The final goal of this course is to have you to implement a small
Routing Protocol to route packets to the correct host.

But first thing first before I scare you with this goal let's learn some cool
stuff.

# What is a Routing Protocol and why do I care?

## Quick definition

A **routing protocol** is a program which _handles network connections_ on a
host to take the incomming packets and _send them to the correct host_.
We call that **"routing"**.

For instance let's say that I want to ping _sktelecom.com_, well first I would
have to find the IP address behind _sktelecom.com_ and for that I will ask my
**DNS** (Domain Name Server).
For the sanity of this example let's say that I have a server at home with a
DNS which knows the IP address of _sktelecom.com_ and it gives me back
**42.42.42.42** (you can check they do own it).

Well now I know the IP address for _sktelecom.com_ but now what? How do I get my
small network packet to go to this IP address ?
Well my friend this is the "magic" behind the internet.

## Waht happen when I send a packet to my grandma

When I send a packet to another host let's say with:
```bash
  bash$ echo "Hello World" | nc 42.42.42.42 4242
```

This packet will be sent to the **default route** defined on my machine.
Most of the time this route is defined by your **DHCP** (Dynamic Host
Configuration Protocol a.k.a the thing that gives you an IP address on your
local network) or the IP is set **statically**.

On your machine you can view it simply by looking at:
```bash
  bash# ip route
  default via 172.17.0.1 dev eth0
  172.17.0.0/16 dev eth0 scope link  src 172.17.0.2
```

Note: In linux eth0 is a device representing your Ethernet port.

Here the **first line** describes the **default host to send the packets to**
(so _172.17.0.1_ which is commonly going to be called *the gateway*).

The **second line** informs us that the network _172.17.0.0/16_ is 
**available on eth0** with a _scope of link_, which means that the network is
valid and **reachable through this device** (eth0).
It's just a fancy way to say that we are connected to the same switch.

If I was to ping an IP adress in this subnet then I would use the **ARP**
protocol (Address Resolution Protocol) which converts an IP address into a MAC
address, MAC address that my local switch can understand.

But this is not the topic of this tutorial.
We are interested about the other cases, **when the address is not reachable in
the link scope** (e.g we are not in the same physical or virtual network).

## Behind the scene

Stay sit you'll understand quite quickly.

When I send my packet to _42.42.42.42:4242_ my network stack will actually
say "Hey I don't know this network, well let's **send it to the default route
and hopefully it will know how to reach it**".
And thus my packet take his little bag and **travel to 172.17.0.1**. But this
computer doesn't know either how to reach _42.42.42.42_... well it will **send
it to it's default route** aswell whatever it can be.

But once there what happens?

```
	    ------------	  -----------
 me ------>| 172.17.0.1 | -----> | 172.0.0.0 | -------> ...?
	    ------------	  -----------
```
_the journey of my packet_

I guess you got it, my packet will just **go up and up in the stack until
someone knows where it can go**. But I don't know of any supercomputer that
routes all the packets in the world, and neither do you. So how does it works??

We need an algorithm to actually **coordinate** the computers so
that they could know about which subnets each other knows.

And that my friend is what a routing protocol is. A network algorithm that
_takes packets_ from a LOT of hosts and _send them_ to some other computer
**which told him he knew the IP address your packet is trying to reach**.

Theses protocols works in coordination and **communicates the routes they know
to one another**.

Amongst the most known we have _BGP, OSPF, IS-IS and RIP_.



# Choose a pokemon

Welcome adventurer, please take a seat and look at my routing protocols, each
of them is special in his way.

But before I let you harvest your desire for knowledge I need to tell you about
the **3 main classes** of Routing protocols:


## 1. Distance-Vector routing protocols:

Theses guys determine the best route between A and B **based on the distance
between A and B**.
For instance let's say that we ahve a network with each letter represetting a
router::

```
A ------- V --------- X --------- Y -------- Z ------- B
	  |					       |
	  ----------- W -------------------- U ---------
```

In this network the shortest distance between A and B is
_"A -> V -> W -> U -> B"_
But the distance can, of course take into account other parameters like the link
latency in order to determine the best route.
But what makes a "Distance-Vector routing protocol" is actually the fact that
each node only knows the paths their direct neighbours offers, i.e
when you look at the informations A will have it will be like:

- To go to B:
  - send packet to V

And informations in V will look like:
- To go to B either:
  - go to **W** for a cost of **3**
  - go to **X** for a cost of **4**

(this is of course assuming the cost of each link is only **1**)

So as you may understand the Distance-Vector routing protocols don't really know
about the whole network, they only know what their neighbours tell them and pass
on this information by incrementing the "hop" counter (a hop being the unit of
distance from one router to another)

## 2. Path-Vector routing protocols

The way Path-Vector routing works is really simillar to Distance vector, the
only difference is that each node, instead of storing the distance to a given
node will instead store... the path, exactly!

If we take back our previous example the node V would have the following
informations:
- To go to B either:
  - go to X -> Y -> Z -> B
  - go to W -> U -> B

Then the protocol can determine which way it wants to take base on it's own
criterias.
The biggest strengh of this protocol compared to Distance-Vector is that you can
choose your route (and this leads to endless features like packet filtering,
redistribution, router blacklisting, etc....)

## 3. Link state routing protocol

The basic concept of a link state protocol is that every node will construct a
graph of the entire network by having each node communicating it's route to one
another.
Once done each node can then calculate the best path with a simple pathfinding
algorithm.

A link-state protocol only pass on informations about the... link status, e.g if
the _connectivity_ between one node and another.

The main characteristic of these protocols is that the entire world knows about
each other neighbours.

I let you satiate your hunger for knowledge by looking over Wikipedia the
definition of RIP, OSPF and BGP where things are really well explained as you
should now be able to understand it with what we just learned.


# Let's get ready to rumble!

In this part we will see how to configure a router with various routing
protocols

## Tutorial BGP

Please go in the bgp folder and run the setup.sh script.
This will setup a few docker containers in an isolated network namespace and
link them together.

If you have a look at the `docker network ls` and `docker ps` commands you might
notice three networks: isp1, is2 and big-cable, and 4 containers: `host1`, `host2`,
`rtr1` and `rtr2`

The topology is the following:

```
		  big-cable
	        (192.168.0.0/24)
	   rtr1 --------------- rtr2
	    |			 |
    isp1    |			 | isp2
(1.0.0.0/24)|			 | (2.0.0.0/24)
	    |			 |
          host1			host2
```
Please have a look at the ./setup.sh to understand how containers are setup,
this will be useful for the next part.

Now that I have these containers setup I need to configure BGP.
The FrrRouting image provides us a terminal which is quite alike to a CISCO
cli. You can run it by typing:
```bash
  $ docker exec -i -t rtr1 /usr/bin/vtysh
```

Now this should drop you in a terminal which is nothing like a shell. By typing
`?` you can shwo the available commands.

I won't go through all the options here because it would take forever but I
suggest you try on some whenever you feel like it.

Our goal now is to configure BGP, to do so we want to enter the config mode, to
do so just type:
```bash
  bash$ configure terminal
```
Note: this (like any command) can be shortened by `conf t`
Then you can run:
```bash
  $ router bgp 1		      # Enables BGP with the AS number 1
  $ bgp router-id 1.0.0.254	      # Creates a router with an ID, (here the
				      # ID is just the IP of this router in it's
				      # own subnet)

  $ neighbor 192.168.0.3 remote-as 2  # Identify the neighbor with the IP
				      # 192.168.0.3 as the AS 2

  $ address-family ipv4 unicast	      # Enter IPv4 config
  $ network 1.0.0.0/24		      # Declare that we own the subnet
				      # 1.0.0.0/24
```

Micro glossary:
- AS: Autonomous System a.k.a a a collection of IP networks and routers under
  the control of one entity. Theses AS number and IP ranges are distributed by
  the IANA.

Now I will let you log to rtr2 and make the correct configuration to declare
2.0.0.0/24 to rtr1.

You can check the config running on the switch at any point in time by running
`show running-config`.

Once done you can verify that it worked by:
- running `show ip bgp summary` when being in non-config mode, should show:
```bash
 IPv4 Unicast Summary:
 BGP router identifier 1.0.0.254, local AS number 1 vrf-id 0
 BGP table version 2
 RIB entries 3, using 576 bytes of memory
 Peers 1, using 14 KiB of memory

 Neighbor        V         AS   MsgRcvd   MsgSent   TblVer  InQ OutQ  Up/Down State/PfxRcd
 192.168.0.3     4          2        20        20        0    0    0 00:15:40            1

 Total number of neighbors 1
```

- entering host1 and trying to send a packet to 2.0.0.42 (host2's IP)
a simple example would be on host1:
```bash
  $ docker exec -i -t host2 sh -c 'socat TCP4-LISTEN:4242 STDOUT'
```

and on host2:
```bash
  $ docker exec -i -t host1 sh -c 'socat STDIN TCP4:1.0.0.42:4242'
  Typing some stuff here that should appear on the other terminal.
  Hello World!
```

If you managed to send/receive stuff CONGRATS! You know how to use BGP to route
packets! Well basically at lease, if you are interested to learn more on the
subject I suggest reading the various RFCs you can find which describe all the
features of the swiss army knife that is BGP.

If you need any extra doc and/or want to go further I advise keeping under your
arm this nice BGP [cheat sheet](https://packetlife.net/media/library/1/BGP.pdf)


## Tutorial OSPF

TODO

## Tutorial RIP

TODO

# Now it's your turn...

Don't worry I won't push you from the cliff without some knowledge about how
you will hit yourself.
Here are some pointers to good documentation about various related topics:
  - [Linux socket manpage](http://man7.org/linux/man-pages/man2/socket.2.html)
  - [This wonderful raw socket tutorial](https://opensourceforu.com/2015/03/a-guide-to-using-raw-sockets/)
  - [THE guide that you should already know] (https://beej.us/guide/bgnet/html/)
  - [Linux's ethernet header struct](https://github.com/spotify/linux/blob/master/include/linux/if_ether.h)
  - [Linux's IP header struct](https://github.com/spotify/linux/blob/master/include/linux/ip.h)

## The basic idea

In order to get you started somewhere I am going to give you some piece of
advices.

1. First start by looking at how to play with RAW sockets.
Theses sockets are going to let you read all the traffic coming from and to
your machine.

2. Learn to unpack Ethernet packets and IP packets.
As you may already know we want to re-route packets to a different IP and
network packets are package, as per the OSI model, in 7 layers.
The only ones that we are interested in are the Layers 2 and 3 (in our case
Ethernet and IP). Once you know how to read an IP packet you can get the
destination address and then you are golden.

3. quickly try to run your code in a dockerized environment.
If you read the scripts from part 1 you should be able to get a small container
running (note: use -v /local/path:/container/path to get your code in). Test
your code in there as you will control the traffic.
Once done you can test this protocol by running a few docker containers like in
the previous example.
