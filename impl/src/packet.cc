#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <linux/ip.h>

#include "packet.hh"
#include "utils.hh"

#define PKT_SIZE sizeof(eth_hdr)+65535

struct iphdr* receive_pkt(int sock_r) {
  // Extract Ethernet header
  struct ethhdr *eth_hdr = (struct ethhdr*)malloc(PKT_SIZE);
  ssize_t len = recv(sock_r, eth_hdr, PKT_SIZE, 0);
  if(len < 0) {
    log(strerror(errno));
    return NULL;
  }
  print_ethernet_hdr(eth_hdr);

  if (!(eth_hdr->h_proto == 8))
    return NULL;
  // FIXME: Drop if packet isn't for us
  // 1- list mac addresses
  // 2- match destination with us

  // Extract IP packet
  struct iphdr *ip = (struct iphdr*)(eth_hdr + 1);
  print_ip_hdr(ip);
  return ip;
}

void print_ethernet_hdr(struct ethhdr *eth){
  printf("\nEthernet Header\n");
  printf("\t|-Source Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n",
      eth->h_source[0], eth->h_source[1], eth->h_source[2], eth->h_source[3],
      eth->h_source[4], eth->h_source[5]);
  printf("\t|-Destination Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n",
      eth->h_dest[0], eth->h_dest[1], eth->h_dest[2], eth->h_dest[3],
      eth->h_dest[4], eth->h_dest[5]);
  printf("\t|-Protocol : %d\n", eth->h_proto);
}

void print_ip_hdr(struct iphdr *ip) {
  unsigned short iphdrlen;
  struct sockaddr_in source, dest;
  memset(&source, 0, sizeof(source));
  source.sin_addr.s_addr = ip->saddr;
  memset(&dest, 0, sizeof(dest));
  dest.sin_addr.s_addr = ip->daddr;

  printf("IP Header\n");
  printf("\t|-Version : %d\n",(unsigned int)ip->version);
  printf("\t|-Internet Header Length : %d DWORDS or %d Bytes\n",
	 (unsigned int)ip->ihl,((unsigned int)(ip->ihl))*4);
  printf("\t|-Type Of Service : %d\n",(unsigned int)ip->tos);
  printf("\t|-Total Length : %d Bytes\n",ntohs(ip->tot_len));
  printf("\t|-Identification : %d\n",ntohs(ip->id));
  printf("\t|-Time To Live : %d\n",(unsigned int)ip->ttl);
  printf("\t|-Protocol : %d\n",(unsigned int)ip->protocol);
  printf("\t|-Header Checksum : %d\n",ntohs(ip->check));
  printf("\t|-Source IP : %s\n", inet_ntoa(source.sin_addr));
  printf("\t|-Destination IP : %s\n",inet_ntoa(dest.sin_addr));
}

