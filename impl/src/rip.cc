#include <sys/types.h>
#include <sys/socket.h>

#include "packet.hh"
#include "utils.hh"

#define RRT 4242

class RRT {
  std::map<> routingTable;

  RRT() {
    // check local subnets on interfaces
    // launch coroutine to advertise the neighbours with them
  }

  void handleRRTPkt() {
    // FIXME
  }

  void tryForwardPkt() {
    // if pkt->din_addr in routingTable
    //	sendPkt

  }

}

class Interfaces {
  Interfaces() {
    // FIXME: IP addresses associated with devices
  }
  bool isPktForUs(struct iphdr *ip) {
    return false;
  }
}

int main() {
  // Open socket
  int sock_r;
  sock_r=socket(AF_PACKET,SOCK_RAW,htons(ETH_P_ALL));
  if (sock_r == -1) {
    log(strerror(errno));
    return -1;
  }
  // receive packet
  struct iphdr *pkt;
  while (true) {
    if (!(pkt = receive_pkt(sock_r)))
      continue;
    if (isPktForUs(pkt) && pkt-pkt->protocol == RRT)
      handleRRTPkt(pkt)
    else
      tryForwardPkt(pkt);
  }
}
