#include <linux/if_ether.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>

#ifndef PACKET_HH
#define PACKET_HH

struct iphdr* receive_pkt(int sock_r);
void print_ethernet_hdr(struct ethhdr* eth);
void print_ip_hdr(struct iphdr *iphdr);

#endif /* !PACKET_HH */

