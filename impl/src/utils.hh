/*
 *  Filename:  utils.hh
 *   Created:  05/25/2020 11:34:43 AM
 *    Author:  Rafael Gozlan <raf@rible.eu>
 */

#include <stdio.h>

#ifndef  UTILS_HH
# define  UTILS_HH

#define log(str) printf("%s:%d: %s\n", __FILE__, __LINE__, str)

#endif   /* !UTILS_HH */

