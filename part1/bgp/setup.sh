#! /bin/bash

docker network create --driver macvlan --internal --subnet 1.0.0.0/24 isp1
docker network create --driver macvlan --internal --subnet 2.0.0.0/24 isp2
docker network create --driver macvlan --internal --subnet 192.168.0.0/24 big-cable

function setup_rtr {
  docker run -t -d --privileged \
    --network isp$1 \
    --ip $1.0.0.254 \
    -v $PWD/rtr$1:/etc/frr/ \
    --name rtr$1 frrouting/frr
  docker network connect big-cable rtr$1
}

function setup_host {
  docker run -t -d --privileged \
    --network isp$1 \
    --ip $1.0.0.42 \
    --name host$1 raf3/bgp-host
  docker exec -it host$1 sh -c "ip route delete default"
  docker exec -it host$1 sh -c "ip route add default via $1.0.0.254"
}

setup_rtr 1
setup_rtr 2
setup_host 1
setup_host 2
