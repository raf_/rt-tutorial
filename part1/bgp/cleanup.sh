#!/bin/bash

docker network disconnect isp1 rtr1
docker network disconnect isp1 host1
docker network disconnect isp2 rtr2
docker network disconnect isp2 host2
docker network disconnect big-cable rtr1
docker network disconnect big-cable rtr2
docker network rm isp1
docker network rm isp2
docker network rm big-cable

docker stop rtr1 rtr2 host1 host2
docker rm rtr1 rtr2 host1 host2
